using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.SignalR;
using Moq;
using MythicTable;
using MythicTable.GameSession;
using Xunit;

namespace LivePlayTests
{
    public class LivePlayHubTests
    {
        [Fact]
        public async Task GameStepIsSubmittedToStateManagerOnce()
        {
            var stateMock = new Mock<IEntityCollection>();
            var hub = new LivePlayHub(stateMock.Object);
            var clientsMock = new Mock<IHubCallerClients<ILiveClient>>();

            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(true));

            clientsMock.Setup(m => m.All).Returns(Mock.Of<ILiveClient>());
            hub.Clients = clientsMock.Object;

            var submittedStep = new SessionDelta
            {
                Entities = new []
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.RebroadcastDelta(submittedStep);

            stateMock.Verify(
                entities => entities.ApplyDelta(
                    It.Is<IEnumerable<EntityOperation>>(step => step == submittedStep.Entities)),
                Times.Once);
        }

        [Fact]
        public async Task GameStepIsRepeatedToAllClientsOnce()
        {
            var stateMock = new Mock<IEntityCollection>();
            var hub = new LivePlayHub(stateMock.Object);
            var clientsMock = new Mock<IHubCallerClients<ILiveClient>>();
            var allClientsMock = new Mock<ILiveClient>();

            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(true));

            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);
            allClientsMock
                .Setup(a => a.ConfirmDelta(It.IsAny<SessionDelta>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;

            var submittedStep = new SessionDelta
            {
                Entities = new []
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.RebroadcastDelta(submittedStep);

            allClientsMock.Verify(
                c => c.ConfirmDelta(It.Is<SessionDelta>(step => step == submittedStep)),
                Times.Once());
        }

        [Fact]
        public async Task FailedSubmissionsAreNotRepeated()
        {
            var stateMock = new Mock<IEntityCollection>();
            var hub = new LivePlayHub(stateMock.Object);
            var clientsMock = new Mock<IHubCallerClients<ILiveClient>>();
            var allClientsMock = new Mock<ILiveClient>();

            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(false));

            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);
            allClientsMock
                .Setup(a => a.ConfirmDelta(It.IsAny<SessionDelta>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;

            var submittedStep = new SessionDelta
            {
                Entities = new []
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.RebroadcastDelta(submittedStep);

            allClientsMock.Verify(
                c => c.ConfirmDelta(It.Is<SessionDelta>(step => step == submittedStep)),
                Times.Never);
        }
    }
}
