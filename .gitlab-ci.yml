variables:
    # When using dind service we need to instruct docker, to talk with the
    # daemon started inside of the service. The daemon is available with
    # a network connection instead of the default /var/run/docker.sock socket.
    #
    # The 'docker' hostname is the alias of the service container as described at
    # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
    #
    # Note that if you're using Kubernetes executor, the variable should be set to
    # tcp://localhost:2375 because of how Kubernetes executor connects services
    # to the job container
    DOCKER_HOST: tcp://docker:2375/
    # When using dind, it's wise to use the overlayfs driver for
    # improved performance.
    DOCKER_DRIVER: overlay2
    CONTAINER_IMAGE: $CI_REGISTRY_IMAGE
    CONTAINER_TAG: $CI_PIPELINE_IID-$CI_COMMIT_SHORT_SHA

stages:
    - build
    - test
    - package
    - containerize
    - deploy

build-client:
    image: node:lts-alpine
    stage: build
    script:
        - cd html
        - npm install
        - npm run build
    artifacts:
        name: "client-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - html/dist/
        expire_in: 1h
    cache:
        key: npm
        paths:
            - html/node_modules/
build-server:
    image: mcr.microsoft.com/dotnet/core/sdk:2.2-alpine
    stage: build
    script:
        - dotnet restore --packages .nuget
        - dotnet build -c Release --no-restore --packages .nuget
    artifacts:
        name: "server-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - server/src/*/bin
            - server/src/MythicTable/obj/project.assets.json
            - server/tests/*/bin
        expire_in: 1h
    cache:
        key: nuget
        paths:
            - .nuget/

test-client:
    stage: test
    image: node:lts-alpine
    dependencies: []
    script:
        - cd html
        - npm run test -- --coverage
    artifacts:
        name: "client-coverage-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - html/coverage/
    cache:
        key: npm
        paths:
            - html/node_modules/

test-server:
    stage: test
    image: mcr.microsoft.com/dotnet/core/sdk:2.2-alpine
    dependencies:
        - build-server
    script:
        - dotnet test

package:
    image: mcr.microsoft.com/dotnet/core/sdk:2.2-alpine
    stage: package
    dependencies:
        - build-server
        - build-client
    script:
        - dotnet publish -c Release --packages .nuget -o ../../../package server/src/MythicTable/
        - cp --archive ./html/dist/* ./package/wwwroot/
    artifacts:
        name: "mythictable-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - package/
    cache:
        key: nuget
        paths:
            - .nuget/

package-docker:
    stage: containerize
    image: docker:stable
    services:
        - docker:dind
    dependencies: [ package ]
    script:
        - docker info
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - "docker build -f Dockerfile.gitlab-ci -t $CONTAINER_IMAGE:$CONTAINER_TAG -t $CONTAINER_IMAGE:latest ."
        - "docker push $CONTAINER_IMAGE:$CONTAINER_TAG"
        - "docker push $CONTAINER_IMAGE:latest"
    only:
        - master

deploy-production:
    stage: deploy
    image:
        name: lachlanevenson/k8s-kubectl:v1.12.7
        entrypoint: []
    dependencies: [ package-docker ]
    tags: [ kubernetes ]
    only:
        kubernetes: active
        refs:
            - master
    environment:
        name: edge
        url: http://edge.mythictable.com
    script:
        - kubectl --token=$KUBE_DEPLOY_TOKEN version
        - kubectl --token=$KUBE_DEPLOY_TOKEN --namespace=mythictable auth can-i patch deployment/mythictable
        - kubectl set image deployment.v1.apps/mythictable mythictable=$CONTAINER_IMAGE:$CONTAINER_TAG --token=$KUBE_DEPLOY_TOKEN --namespace=mythictable
