const entities = [
    {
        id: 'ranger',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 17, r: 20 }
        },
        asset: { kind: 'image', src: "/static/assets/Ranger.png" },
    },
    {
        id: 'redcap',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 20, r: 19 }
        },
        asset: { kind: 'image', src: "/static/assets/Redcap.png" },
    },
    {
        id: 'wolf',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 9, r: 7 }
        },
        asset: { kind: 'image', src: "/static/assets/Wolf.png" },
    },
    {
        id: 'tauren',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 12, r: 24 }
        },
        asset: { kind: 'image', src: "/static/assets/Tauren.png" }
    },
    {
        id: 'red',
        token: {
            image: { color: 'red' },
            scene: 'stronghold',
            pos: { q: 0, r: 0 }
        }
    },
    {
        id: 'green',
        token: {
            image: { color: 'green' },
            scene: 'stronghold',
            pos: { q: 5, r: 4 }
        }
    },
    {
        id: 'blue',
        token: {
            image: { color: 'blue' },
            scene: 'stronghold',
            pos: { q: 7, r: 10 }
        }
    },
    {
        id: 'teal',
        token: {
            image: { color: 'teal'},
            scene: 'stronghold',
            pos: { q: 3, r: 5 }
        }
    },

    // Stages/scenes ----------------------------
    {
        id: 'stronghold',
        scene: { stage: '.' },
        stage: {
            grid: { type: 'square', size: 50 },
            bounds: {
                nw: { q: -1, r: -1 },
                se: { q: 60, r: 60}
            },
            color: '#223344',
            elements: [
                {
                    id: 'background',
                    asset: 'stronghold_bg',
                    pos: { q: 0, r: 0, pa: '00' }
                }
            ]
        }
    },

    // Plain assets --------------------------
    {
        id: 'stronghold_bg',
        asset: { kind: 'image', src: "/static/assets/stronghold.jpg" },
    }
]

const loader = async (store) => {
    await store.dispatch('entities/load', entities);
}

export { loader, entities };
